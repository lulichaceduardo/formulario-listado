import React, { Component } from 'react';
import Cita from './Cita'

class ListaCitas extends Component {
  render() {
    const {listaCitas, eliminarCita} = this.props
    const element = listaCitas.length>0? <h3>Listado de citas</h3>:
                    <h3>Sin reservas</h3>
    return (
      <section className="jumbotron">
          {element}
          <div className="card-deck">
            {listaCitas.map((cita)=>{
              return (
                <Cita 
                  key={cita.id} 
                  cita={cita} 
                  eliminarCita={eliminarCita}/>
              )
            })}
            
          </div>
        </section>
    );
  }
}

export default ListaCitas;